let
  config = import ./config.nix;
  pkgs = config.pkgs;
  # pkgs-unstable = config.pkgs-unstable;
  postgres = import ./services/postgres.nix;
  invidious = import ./services/invidious.nix;
  #nitter = import ./services/nitter.nix;
  libreddit = import ./services/libreddit.nix;
  traefik = import ./services/traefik.nix;
  searxng = import ./services/searx.nix;
  grafana-loki = import ./services/grafana-loki.nix;
  rabbitmq = import ./services/rabbitmq.nix;
  airsonic = import ./services/airsonic.nix;
  snapcast = import ./services/snapcast.nix;
  mopidy = import ./services/mopidy.nix;
  mpd = import ./services/mpd.nix;
  snapcast-volume-ui = import ./services/snapcast-volume-ui.nix;
  soapbox = import ./services/soapbox.nix;
  pulseaudio = import ./services/pulseaudio.nix;
  opensearch = import ./services/opensearch.nix;
  elasticsearch = import ./services/elasticsearch.nix;
in rec {
  serviceimages = pkgs.writeText "images.ini" ''
    [containers]
    invidious=${invidious(pkgs)}
    postgres=${postgres(pkgs)}
    traefik=${traefik(pkgs)}
    libreddit=${libreddit(pkgs)}
    searxng=${searxng(pkgs)}
    loki=${grafana-loki(pkgs)}
    rabbitmq=${rabbitmq(pkgs)}
    airsonic=${airsonic(pkgs)}
    snapcast=${snapcast(pkgs)}
    mopidy=${mopidy(pkgs)}
    mpd=${mpd(pkgs)}
    snapcast_volume_ui=${snapcast-volume-ui(pkgs)}
    soapbox=${soapbox(pkgs)}
    pulseaudio=${pulseaudio(pkgs)}
    elasticsearch=${elasticsearch(pkgs)}
    opensearch=${opensearch(pkgs)}
  '';
}
