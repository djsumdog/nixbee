# nixbee

Initial repository for experimenting with a system for building and deploying services using Nix


```
python3 nixbuild.py
docker-compose -H "ssh://cassius@natalie" --env-file image.env up -d

poetry run python nixbuild.py; and /usr/libexec/docker/cli-plugins/docker-compose -H "ssh://cassius@natalie" --env-file image.env up -d --remove-orphan; and notify-send "done1"

poetry run python nixbuild.py; and docker compose -H "ssh://cassius@natalie" --env-file image.env up -d; and notify-send "done1"
```
