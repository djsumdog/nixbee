let
  config = import ./config.nix;
  pkgs = config.pkgs;

  # nixos-22.05 / https://status.nixos.org/
  # pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/d86a4619b7e80bddb6c01bc01a954f368c56d1df.tar.gz") {};


  namecom-json = pkgs.writeTextFile {
    name = "certbot-dns-namecom-template";
    destination = "/etc/certbot-dns-namecom.config.json.template";
    text = ''
    {
      "namecom": {
        "username": $username,
        "token": $token
      },
      "waitsec": 30
    }
    '';
  };

  env-shim = pkgs.runCommand "env-shim" {} ''
    mkdir -p $out/usr/bin
    ln -s ${pkgs.coreutils}/bin/env $out/usr/bin/env
  '';

  startup-cmd = pkgs.writeTextFile {
    name = "startup";
    destination = "/startup";
    executable = true;
    text = ''
    #!/bin/sh
    set -e
    jq --null-input --arg username "$USERNAME" --arg token "$TOKEN" \
      "$(cat /etc/certbot-dns-namecom.config.json.template)" \
      > /etc/certbot-dns-namecom.config.json && \
    /bin/certbot certonly --manual --manual-auth-hook /src/certbot-dns-namecom.py \
      --preferred-challenges dns -d "$DOMAIN" -d "*.$DOMAIN" --email "$EMAIL" --agree-tos && \
    /src/certbot-dns-namecom.py clean
    '';
  };

  certbot-dns-namecom = fetchGit {
    url = "https://github.com/laonan/certbot-dns-name-com.git";
    ref = "master";
    rev = "cdb98b67e873ee26c9b70593dae23aa19c0df60b";
  };

  python3 = pkgs.python310;
  python3-with-packages = python3.withPackages (p: with p; [
      requests
  ]);
in rec {  

  docker = pkgs.dockerTools.buildImage {
    name = "certbot-namedns";
    contents = [ certbot-dns-namecom startup-cmd namecom-json python3-with-packages env-shim pkgs.jq pkgs.coreutils pkgs.busybox pkgs.bash pkgs.certbot ];
    config = {
      Cmd = [ "/startup" ];
      Env = [ "USERNAME=username" "TOKEN=token" "DOMAIN=example.com" "EMAIL=noone@example.com" "PYTHONPATH=${python3-with-packages}/${python3-with-packages.sitePackages}" ];
      Volumes = {
        "/var/log/letsencrypt/" = {};
        "/etc/letsencrypt" = {};
      };
    };
  };
}
