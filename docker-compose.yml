---
volumes:
  certs:
  loki:
  rabbitmq:
  airsonic:
  snapcast:
  mopidy:
  mpd:
services:
  traefik:
    image: ${TRAEFIK_IMAGE}
    command:
      - "--providers.docker"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.websecure.address=:443"
      - "--entrypoints.web.http.redirections.entryPoint.to=websecure"
      - "--entrypoints.web.http.redirections.entryPoint.scheme=https"
      - "--entrypoints.web.http.redirections.entrypoint.permanent=true"
      - "--certificatesresolvers.pfresolver.acme.dnschallenge=true"
      - "--certificatesresolvers.pfresolver.acme.dnschallenge.provider=namedotcom"
      - "--certificatesresolvers.pfresolver.acme.dnschallenge.delaybeforecheck=0"
      - "--certificatesresolvers.pfresolver.acme.email=notify@battlepenguin.com"
      - "--certificatesresolvers.pfresolver.acme.storage=/certs/acme.json"
      - "--api=true"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - certs:/certs
    ports:
      - "80:80"
      - "443:443"
    environment:
      - NAMECOM_USERNAME
      - NAMECOM_API_TOKEN
    labels:
      traefik.http.routers.api.rule: "Host(`lb.web.penguin.farm`)"
      traefik.http.routers.api.service: "api@internal"
      traefik.http.routers.api.middlewares: "auth"
      traefik.http.routers.api.tls.certresolver: "pfresolver"
      traefik.http.routers.api.entrypoints: "websecure"
      traefik.http.middlewares.auth.basicauth.users: ${TRAEFIK_AUTH}
  nitter:
    image: ${NITTER_IMAGE}
    labels:
      traefik.http.routers.nitter.entrypoints: "websecure"
      traefik.http.routers.nitter.rule: "Host(`nitter.web.penguin.farm`)"
      traefik.http.routers.nitter.tls.certresolver: "pfresolver"
      traefik.http.services.nitter.loadbalancer.server.port: "8182"
  libreddit:
    image: ${LIBREDDIT_IMAGE}
    labels:
      traefik.http.routers.libreddit.entrypoints: "websecure"
      traefik.http.routers.libreddit.rule: "Host(`libreddit.web.penguin.farm`)"
      traefik.http.routers.libreddit.tls.certresolver: "pfresolver"
      traefik.http.services.libreddit.loadbalancer.server.port: "8080"
  searx:
    image: ${SEARX_IMAGE}
    labels:
      traefik.http.routers.searx.entrypoints: "websecure"
      traefik.http.routers.searx.rule: "Host(`searx.web.penguin.farm`)"
      traefik.http.routers.searx.tls.certresolver: "pfresolver"
      traefik.http.services.searx.loadbalancer.server.port: "8888"
  loki:
    image: ${LOKI_IMAGE}
    labels:
      traefik.http.routers.loki.entrypoints: "websecure"
      traefik.http.routers.loki.rule: "Host(`loki.web.penguin.farm`)"
      traefik.http.routers.loki.tls.certresolver: "pfresolver"
      traefik.http.services.loki.loadbalancer.server.port: "3100"
    volumes:
      - loki:/loki
  opensearch:
    image: ${OPENSEARCH_IMAGE}
    ports:
      - "9200:9200"
    volumes:
      - /mnt/data/opensearch-fediverse/node-1:/opensearch
  rabbitmq:
    image: ${RABBITMQ_IMAGE}
    hostname: rabbitmq
    ports:
      - "5672:5672"
    labels:
      traefik.http.routers.rabbitmq.entrypoints: "websecure"
      traefik.http.routers.rabbitmq.rule: "Host(`rabbitmq.web.penguin.farm`)"
      traefik.http.routers.rabbitmq.tls.certresolver: "pfresolver"
      traefik.http.services.rabbitmq.loadbalancer.server.port: "15672"
    volumes:
      - rabbitmq:/rabbitmq
  airsonic:
    image: ${AIRSONIC_IMAGE}
    labels:
      traefik.http.routers.airsonic.entrypoints: "websecure"
      traefik.http.routers.airsonic.rule: "Host(`airsonic.web.penguin.farm`)"
      traefik.http.routers.airsonic.tls.certresolver: "pfresolver"
      traefik.http.services.airsonic.loadbalancer.server.port: "8080"
    volumes:
      - airsonic:/airsonic
      - /media/Music:/music:ro
  snapcast:
    image: ${SNAPCAST_IMAGE}
    hostname: natalie
    volumes:
      - snapcast:/snapcast
    ports:
      - "1704:1704"
      - "1705:1705"
  mopidy:
    image: ${MOPIDY_IMAGE}
    labels:
      traefik.http.routers.mopidy.entrypoints: "websecure"
      traefik.http.routers.mopidy.rule: "Host(`music.web.penguin.farm`)"
      traefik.http.routers.mopidy.tls.certresolver: "pfresolver"
      traefik.http.services.mopidy.loadbalancer.server.port: "6680"
    volumes:
      - snapcast:/snapcast
      - mopidy:/mopidy
      - /media/Music:/music:ro
  mpd:
    image: ${MPD_IMAGE}
    volumes:
      - snapcast:/snapcast
      - mpd:/mpd
      - /media/Music:/music:ro
    ports:
      - "6600:6600"
  snapcast-volume-ui:
    image: ${SNAPCAST_VOLUME_UI_IMAGE}
    labels:
      traefik.http.routers.snapcast-volume-ui.entrypoints: "websecure"
      traefik.http.routers.snapcast-volume-ui.rule: "Host(`volume.web.penguin.farm`)"
      traefik.http.routers.snapcast-volume-ui.tls.certresolver: "pfresolver"
      traefik.http.services.snapcast-volume-ui.loadbalancer.server.port: "1705"
  pulseaudio:
    image: ${PULSEAUDIO_IMAGE}
    volumes:
      - snapcast:/snapcast
    ports:
      - "4713:4713"


networks:
  default:
    # Taken from: https://bytemeta.vip/repo/containers/podman/issues/15580
    # Comment lines below if using for the 1st time
    # external: true
    # name: nixbee_default
