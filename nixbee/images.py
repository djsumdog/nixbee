# Written by Sumit Khanna
# License: AGPLv3
# https://battlepenguin.com
import logging
import subprocess
import sys
from typing import List
import docker
import json
import tarfile

log = logging.getLogger('nixbee')


def build_nix(nix_file: str, containers_ini: str) -> None:
    log.info(f'Building Nix Containers {nix_file}')
    cmd = ['nix-build', nix_file, '-o', containers_ini]
    nix_build_proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = nix_build_proc.communicate()
    if nix_build_proc.returncode != 0:
        log.error(f"Error running nix-build on {nix_file}")
        for line in stdout.decode('utf-8').split('\n'):
            log.error(line)
        for line in stderr.decode('utf-8').split('\n'):
            log.error(line)
        sys.exit(2)
    else:
        log.info('nix-build complete')
        for line in stdout.decode('utf-8').split('\n'):
            log.debug(line)
        for line in stderr.decode('utf-8').split('\n'):
            log.debug(line)


def loaded_images(docker_client: docker.DockerClient) -> List[str]:
    images = docker_client.images.list()
    hashes = []
    for image in images:
        for tag in image.tags:
            if tag.startswith('localhost/'):
                hashes.append(tag.split('/')[-1])
            else:
                hashes.append(tag)
    return hashes


def repo_tag_from_container_tar_gz(tar_gz_file):
    with tarfile.open(tar_gz_file, "r:gz") as tar:
        # TODO: error handling
        member = tar.getmember('manifest.json')
        fd = tar.extractfile(member)
        manifest_json = json.load(fd)
        return manifest_json[0]['RepoTags'][0]
