# Written by Sumit Khanna
# License: AGPLv3
# https://battlepenguin.com
from configparser import ConfigParser

import yaml
import click
import logging
from nixbee.build import build_nix_containers


log = logging.getLogger('nixbee')


def load_config(config_file):
    with open(config_file, 'r') as fd:
        y_config = yaml.safe_load(fd)
    return y_config


@click.group()
@click.option('--config', default='config.yaml', help='Configuration YAML File')
@click.option('--dry-run', is_flag=True, default=False, help='Do not perform updates')
@click.option('--verbose/--silent', default=False, help='Additional diagnostics and logging')
@click.pass_context
def main(ctx, config, dry_run, verbose):
    ctx.ensure_object(dict)
    # ctx.obj['config'] = load_config(config)
    # if not validate_config(ctx.obj['config']):
    #     sys.exit(2)
    ctx.obj['dry_run'] = dry_run

    if verbose:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)


@main.command()
@click.option('--config', default='config.yaml', help='Configuration YAML File')
@click.pass_context
def build(ctx, config):
    containers_ini_file = 'containers.ini'
    docker_socket = 'ssh://<IP>'
    build_nix_containers(containers_ini_file, docker_socket)


if __name__ == '__main__':
    main()
