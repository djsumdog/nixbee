# Written by Sumit Khanna
# License: AGPLv3
# https://battlepenguin.com
import gzip
import logging
import sys
from configparser import ConfigParser
import docker
from nixbee.images import build_nix, loaded_images, repo_tag_from_container_tar_gz

log = logging.getLogger('nixbee')


def build_nix_containers(containers_ini_file: str, docker_socket_url: str):
    build_nix("services.nix", containers_ini_file)
    container_cfg = ConfigParser()
    container_cfg.read(containers_ini_file)

    if 'containers' not in container_cfg:
        log.error(f"{containers_ini_file} does not contain [containers] section")
        sys.exit(3)

    image_env = {}
    docker_client = docker.DockerClient(base_url=docker_socket_url)
    version = docker_client.version()
    log.debug(f'Release: {version["Version"]}')
    log.debug(f'Compatible API {version["ApiVersion"]}')
    log.debug(f'docker API: {version["Components"][0]["Details"]["ApiVersion"]}')

    existing_images = loaded_images(docker_client)

    for c in container_cfg['containers']:
        tar_file = container_cfg.get('containers', c)
        tar_image_tag = repo_tag_from_container_tar_gz(tar_file)

        if tar_image_tag in existing_images:
            log.info(f'Image {tar_image_tag} exists, skipping...')
            image_env[c] = f'localhost/{tar_image_tag}'
        else:
            log.info(f"Loading service {c} with container {tar_file}")
            container_tar = gzip.open(tar_file)
            result = docker_client.images.load(container_tar)
            image = [r.tags[0] for r in result][0]
            image_env[c] = image

    with open('secrets.env', 'r') as fd:
        secrets = fd.read()

    with open('image.env', 'w') as fd:
        fd.write(f"{secrets}\n")
        for name, image in image_env.items():
            fd.write(f"{name.upper()}_IMAGE={image}\n")
