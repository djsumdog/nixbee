{ pkgs, lib, nimPackages, fetchFromGitHub, ... }:
with pkgs;
let

  nitter-git = nimPackages.buildNimPackage {
  pname = "nitter";
  version = "unstable-2022-10-17";

  src = fetchFromGitHub {
    owner = "zedeus";
    repo = "nitter";
    rev = "2ac3afa5b273a502d7632e9346c7c3bc9283fb48";
    hash = "sha256-fdzVfzmEFIej6Kb/K9MQyvbN8aN3hO7RetHL53cD59k=";
  };

  buildInputs = with nimPackages; [
    flatty
    jester
    jsony
    karax
    markdown
    nimcrypto
    packedjson
    redis
    redpool
    sass
    supersnappy
    zippy
  ];

  nimBinOnly = true;

  nimFlags = ["-d:ssl"];

  postBuild = ''
    nim c --hint[Processing]:off -r tools/gencss
    nim c --hint[Processing]:off -r tools/rendermd
  '';

  postInstall = ''
    mkdir -p $out/share/nitter
    cp -r public $out/share/nitter/public
  '';

  meta = with lib; {
    homepage = "https://github.com/zedeus/nitter";
    description = "Alternative Twitter front-end";
    license = licenses.agpl3Only;
    maintainers = with maintainers; [ erdnaxe ];
    mainProgram = "nitter";
  };
};

  redis-run = writeTextFile {
      name = "redis-run";
      executable = true;
      destination = "/etc/s6/redis/run";
      text = ''
      #!/bin/sh
      echo "Starting Redis on 7777"
      exec redis-server --port 7777
      '';
  };
  redis-finish = writeTextFile {
      name = "redis-finish";
      executable = true;
      destination = "/etc/s6/redis/finish";
      text = ''
      #!/bin/sh
      echo "Stopping Redis"
      '';
  };
  nitter-run = writeTextFile {
      name = "nitter-run";
      executable = true;
      destination = "/etc/s6/nitter/run";
      text = ''
      #!/bin/sh
      # ln -s /etc/ssl/certs/ca-bundle.crt /etc/ssl/ca-bundle.pem
      echo "Starting Nitter"
      exec nitter
      '';
  };
  nitter-finish = writeTextFile {
      name = "nitter-finish";
      executable = true;
      destination = "/etc/s6/nitter/finish";
      text = ''
      #!/bin/sh
      echo "Stopping Nitter"
      '';
  };
  nitter-conf = writeTextFile {
      name = "nitter-conf";
      executable = true;
      destination = "/etc/nitter.conf";
      text = ''
        [Server]
        address = "0.0.0.0"
        port = 8182
        https = false  # disable to enable cookies when not using https
        httpMaxConnections = 100
        staticDir = "/share/nitter/public"
        title = "nitter"
        hostname = "natalie"

        [Cache]
        listMinutes = 240  # how long to cache list info (not the tweets, so keep it high)
        rssMinutes = 10  # how long to cache rss queries
        redisHost = "localhost"  # Change to "nitter-redis" if using docker-compose
        redisPort = 7777
        redisPassword = ""
        redisConnections = 20  # connection pool size
        redisMaxConnections = 30
        # max, new connections are opened when none are available, but if the pool size
        # goes above this, they're closed when released. don't worry about this unless
        # you receive tons of requests per second

        [Config]
        hmacKey = "oogha1ohdeiR1ooquacoh7ooshuseiQuudohlooYu1cho"  # random key for cryptographic signing of video urls
        base64Media = false  # use base64 encoding for proxied media urls
        enableRSS = true  # set this to false to disable RSS feeds
        enableDebug = false  # enable request logs and debug endpoints
        proxy = ""  # http/https url, SOCKS proxies are not supported
        proxyAuth = ""
        tokenCount = 10
        # minimum amount of usable tokens. tokens are used to authorize API requests,
        # but they expire after ~1 hour, and have a limit of 187 requests.
        # the limit gets reset every 15 minutes, and the pool is filled up so there's
        # always at least $tokenCount usable tokens. again, only increase this if
        # you receive major bursts all the time

        # Change default preferences here, see src/prefs_impl.nim for a complete list
        [Preferences]
        theme = "Nitter"
        replaceTwitter = "nitter.web.penguin.farm"
        replaceYouTube = "piped.kavin.rocks"
        replaceReddit = "libreddit.web.penguin.farm"
        replaceInstagram = ""
        proxyVideos = true
        hlsPlayback = true
        infiniteScroll = false
      '';
  };
in
  dockerTools.buildLayeredImage {
    name = "nitter";
    contents = [ nitter-git s6 redis busybox cacert
                 redis-run redis-finish nitter-run nitter-finish
                 nitter-conf ];
    config = {
      Entrypoint = [ "/bin/s6-svscan" "/etc/s6" ];
      Env = [ "NITTER_CONF_FILE=/etc/nitter.conf" ];
      Volumes = {};
    };
  }
