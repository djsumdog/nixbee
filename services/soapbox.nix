{ pkgs, ... }:
let
  nginx-run = pkgs.writeTextFile {
      name = "nginx-run";
      executable = true;
      destination = "/etc/s6/nginx/run";
      text = ''
      #!/bin/sh
      echo "Starting nginx on 7777"
      exec nginx-server --port 7777
      '';
  };
  nginx-finish = pkgs.writeTextFile {
      name = "nginx-finish";
      executable = true;
      destination = "/etc/s6/nginx/finish";
      text = ''
      #!/bin/sh
      echo "Stopping nginx"
      '';
  };
  nginx-config = pkgs.writeTextFile {
      name = "nginx-config";
      destination = "/etc/nginx.conf";
      text = ''
      '';
  };

  soapbox-ui-src = fetchGit {
    url = "https://gitlab.com/soapbox-pub/soapbox.git";
    ref = "refs/tags/v3.2.0";
    rev = "c108c8cfbd69ecd0efbfed2af400a8587d6f0f98";
  };

  soapbox-dir = pkgs.runCommand "" {
    paths = [ soapbox-ui-src ];
  } ''
    mkdir -p $out/soapbox-ui
    cp -r ${soapbox-ui-src}/* $out/soapbox-ui
    '';
in
pkgs.dockerTools.buildLayeredImage {
  name = "pleroma";
  contents = [ pkgs.pleroma pkgs.cacert pkgs.busybox pkgs.nginx soapbox-dir
               nginx-run nginx-config nginx-finish ];
  config = {
    Entrypoint = [ "pleroma" ];
    Env = [];
    Volumes = {
      "/pleroma" = {};
    };
  };
}
