{ pkgs, ... }:
pkgs.dockerTools.buildLayeredImage {
  name = "traefik";
  contents = [ pkgs.traefik pkgs.cacert ];
  config = {
    Entrypoint = [ "traefik" ];
    Env = [];
    Volumes = {
      "/var/run/docker.sock" = {};
    };
  };
}

