{ pkgs, ... }:
pkgs.dockerTools.buildLayeredImage {
  name = "rabbitmq";
  contents = [ pkgs.rabbitmq-server pkgs.busybox pkgs.locale pkgs.glibcLocales ];
  config = {
    Entrypoint = [ "rabbitmq-server" ];
    Env = [ "RABBITMQ_ENABLED_PLUGINS_FILE=/rabbitmq/enabled_plugins"
            "RABBITMQ_MNESIA_BASE=/rabbitmq/mnesia"
            "RABBITMQ_PID_FILE=/rabbitmq/rabbitmq.pid"
            "RABBITMQ_LOG_BASE=/rabbitmq/log"
            "LANG=en_US.utf8"
            "LOCALE_ARCHIVE=${pkgs.glibcLocales.out}/lib/locale/locale-archive"
          ];
    Volumes = { "/rabbitmq" = {}; };
  };
}
