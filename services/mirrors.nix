{ pkgs, ... }:
let
  sync-script = pkgs.writeTextFile {
      name = "sync-script";
      destination = "/sync-mirrors";
      text = ''
      #!/bin/bash
      set -e
      '';
  };
in
pkgs.dockerTools.buildLayeredImage {
  name = "loki";
  contents = [ pkgs.rsync pkgs.busybox sync-script ];
  config = {
    Entrypoint = [ "sleep" "100000" ];
    Env = [];
    Volumes = { "/mirrors" = {}; };
  };
}
