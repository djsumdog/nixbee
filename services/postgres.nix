{ pkgs, ... }:
with pkgs;
let
  entryPoint = writeShellScriptBin "entrypoint.sh" ''
    #!/bin/sh
    set -Eeuo pipefail

    PGUSER=${PGUSER:-postgres}
    PGDATA="/postgres"
    export PGDATA

    if [[ ! -d "$PGDATA" ]]; then
      mkdir -p "$PGDATA"
      initdb --auth=trust --encoding=UTF8 --no-locale -U "$PGUSER"

      cat >> "$PGDATA/postgresql.conf" <<-EOF
        listen_addresses = '*'
        unix_socket_directories = '$PGDATA'
    EOF
    fi

    exec postgres

  '';
in
  dockerTools.buildLayeredImage {
    name = "postgres";
    contents = [ busybox bash postgresql_14 entryPoint ];
    config = {
      Cmd = [ "entrypoint.sh" ];
      Env = [];
      Volumes = { "/postgres" = {}; };
    };
  }

