{ pkgs, lib, ... }:
let
mopidy-config = pkgs.writeTextFile {
    name = "mopidy-config";
    destination = "/etc/mopidy.conf";
    text = ''
    [core]
    cache_dir = /mopidy/cache
    config_dir = /mopidy/config
    data_dir = /mopidy/data
    max_tracklist_length = 900000
    restore_state = false

    [logging]
    verbosity = 0
    format = %(levelname)-8s %(asctime)s [%(process)d:%(threadName)s] %(name)s\n  %(message)s
    color = true
    config_file =

    [audio]
    mixer = software
    mixer_volume =
    #output = autoaudiosink
    output = audioresample ! audioconvert ! audio/x-raw,rate=48000,channels=2,format=S16LE ! filesink location=/snapcast/snapfifo
    buffer_time =

    [file]
    enabled = false

    [local]
    enabled = true
    media_dir = /music
    max_search_results = 900000
    timeout = 120

    [http]
    enabled = true
    hostname = ::
    port = 6680
    zeroconf = Mopidy HTTP server on $hostname
    allowed_origins =
    csrf_protection = true
    default_app = mopidy

    [m3u]
    enabled = true
    base_dir = /music
    default_encoding = latin-1
    default_extension = .m3u8
    playlists_dir = /mopidy/playlists

    [softwaremixer]
    enabled = true

    [stream]
    enabled = true
    protocols =
      http
      https
      mms
      rtmp
      rtmps
      rtsp
    metadata_blacklist =
    timeout = 5000

    [mpd]
    enabled = false
    hostname = ::
    port = 6600
    password = Mus1cMan
    max_connections = 20
    connection_timeout = 60
    zeroconf = Mopidy MPD server on $hostname
    command_blacklist =
      listall
      listallinfo

    '';
};

mopidy-startup = pkgs.writeTextFile {
    name = "mopidy-startup";
    executable = true;
    destination = "/bin/mopidy-startup";
    text = ''
    #!/bin/sh
    echo "Loading SSL Library Hooks"
    source /nix-support/setup-hook
    echo "Current Environment:"
    env
    echo "Starting Mopidy"
    exec mopidy --config /etc/mopidy.conf
    '';
};

mopidy-with-extensions = pkgs.buildEnv {
  name = "mopidy-with-extensions";
  paths = lib.closePropagation [ pkgs.mopidy-mpd pkgs.mopidy-local pkgs.mopidy-podcast pkgs.mopidy-youtube pkgs.mopidy-moped pkgs.mopidy-iris pkgs.mopidy-muse ];
  pathsToLink = [ "/${pkgs.mopidyPackages.python.sitePackages}" ];
  nativeBuildInputs = [ pkgs.makeWrapper ];
  postBuild = ''
    makeWrapper ${pkgs.mopidy}/bin/mopidy $out/bin/mopidy \
      --prefix PYTHONPATH : $out/${pkgs.mopidyPackages.python.sitePackages}
  '';
};

in
pkgs.dockerTools.buildLayeredImage {
  name = "mopidy";
  contents = [ mopidy-with-extensions mopidy-config mopidy-startup pkgs.busybox pkgs.cacert pkgs.openssl ];
  config = {
    Entrypoint = [ "/bin/mopidy-startup" ];
    #Entrypoint = [ "sleep" "100000" ];
    Volumes = {
      "/snapcast" = {};
      "/music" = {};
    };
  };
}
