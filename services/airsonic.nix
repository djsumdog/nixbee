{ pkgs, ... }:
pkgs.dockerTools.buildLayeredImage {
  name = "airsonic";
  contents = [ pkgs.airsonic pkgs.jre8 (pkgs.runCommand "tmp-dir" {} ''mkdir -p $out/tmp; chmod 1777 $out/tmp'')];
  config = {
    Entrypoint = [ "java" "-Dairsonic.home=/airsonic" "-jar" "${pkgs.airsonic.out}/webapps/airsonic.war" ];
    Env = [];
    Volumes = { "/airsonic" = {}; };
  };
}
