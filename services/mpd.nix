{ pkgs, ... }:
let
mpd-config = pkgs.writeTextFile {
    name = "mpd-config";
    destination = "/etc/mpd.conf";
    text = ''
      music_directory "/music"
      playlist_directory "/mpd/playlists"
      db_file   "/mpd/mpd.db"
      log_file   "/mpd/log"
      pid_file   "/mpd/run.pid"
      state_file   "/mpd/state"

      bind_to_address "0.0.0.0"
      port    "6600"
      password   "Mus1cMan@read,add,control,admin"
      input {
              plugin "curl"
      }

      audio_output {
          type            "fifo"
          name            "snapcast"
          path            "/snapcast/mpd_snap"
          format          "48000:16:2"
          mixer_type      "software"
      }

      max_playlist_length  "200000"
      filesystem_charset "UTF-8"
    '';
};
in
pkgs.dockerTools.buildLayeredImage {
  name = "mpd";
  contents = [ pkgs.busybox mpd-config pkgs.mpd ];
  config = {
    # Entrypoint = [ "sleep" "1d" ];
    Entrypoint = [ "mpd" "--no-daemon" "/etc/mpd.conf" ];
    Env = [];
    Volumes = {
      "/music" = {};
      "/mpd" = {};
    };
  };
}
