{ pkgs, ... }:
let
pulseaudio-config = pkgs.writeTextFile {
    name = "pulseaudio-config";
    destination = "/var/run/pulse/.config/pulse";
    text = ''
    load-module module-native-protocol-unix auth-anonymous=1
    load-module module-native-protocol-tcp  auth-anonymous=1
    load-module module-pipe-sink file=/snapcast/snapfifo sink_name=Snapcast format=s16le rate=48000
    update-sink-proplist Snapcast device.description=Snapcast
    '';
};
pulse-startup = pkgs.writeTextFile {
    name = "pulse-startup";
    executable = true;
    destination = "/bin/pulse-startup";
    text = ''
    #!/bin/sh

    pulse_setup() {
      sleep 10;
      echo "Running Pulse Setup"
      su - pulse -c \
        "pactl load-module module-native-protocol-tcp  auth-anonymous=1;
        pactl load-module module-pipe-sink file=/snapcast/pulse_snap sink_name=Snapcast format=s16le rate=48000"
    }

    echo "Pulse Setup"
    touch /etc/passwd
    touch /etc/group
    mkdir -p /var/lib
    mkdir -p /tmp
    chmod 0777 /tmp
    mkdir -p /var/run/pulse/.config/pulse
    adduser -h /var/run/pulse pulse
    chown -R pulse:pulse /var/run/pulse
    #addgroup pulse
    #addgroup pulse-access
    # echo "qwertyuiopasdfghjkl" > /var/run/pulse/.config/pulse/cookie
    pulse_setup &
    exec pulseaudio --daemonize=false --system --disallow-exit
    '';
};
in
pkgs.dockerTools.buildLayeredImage {
  name = "pulseaudio";
  contents = [ pkgs.busybox pkgs.pulseaudio pulse-startup ];
  config = {
    # Entrypoint = [ "pulseaudio" "--daemonize=false" "--system" "--disallow-exit" ];
    # Entrypoint = [ "sleep" "1d" ];
    Entrypoint = [ "/bin/pulse-startup" ];
    Env = [];
    Volumes = { "/snapcast" = {}; };
  };
}
