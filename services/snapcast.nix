{ pkgs, ... }:
let
snapcast-config = pkgs.writeTextFile {
    name = "snapcast-config";
    destination = "/snapserver.conf";
    text = ''
    [server]
    pidfile = /snapcast/pid
    datadir = /snapcast/

    [http]
    enabled = true
    bind_to_address = 0.0.0.0
    bind_to_address = ::
    port = 1780

    doc_root = /usr/share/snapserver/snapweb
    host = music.web.penguin.farm

    [tcp]
    enabled = true
    bind_to_address = 0.0.0.0
    bind_to_address = ::

    [stream]
    #bind_to_address = 0.0.0.0
    #port = 1704
    source = pipe:///snapcast/pulse_snap?name=netpulse
    source = pipe:///snapcast/mpd_snap?name=mpd
    source = meta:///netpulse/mpd/?name=all
    #source = tcp://127.0.0.1?name=mopidy_tcp

    buffer = 200
    #send_to_muted = false

    [logging]
    sink = stdout

    # log filter <tag>:<level>[,<tag>:<level>]*
    # with tag = * or <log tag> and level = [trace,debug,info,notice,warning,error,fatal]
    #filter = *:info
    '';
};
in
pkgs.dockerTools.buildLayeredImage {
  name = "snapcast";
  contents = [ pkgs.busybox pkgs.snapcast snapcast-config ];
  config = {
    Entrypoint = [ "snapserver" "--config" "/snapserver.conf" ];
    /* Entrypoint = [ "snapserver"
                   "-s" "pipe:///snapcast/mpd_snap?name=MPD"
                   "-s" "pipe:///snapcast/pulse_snap?name=Netpulse"
                   "-s" "meta:/Netpulse/MPD/?name=default"
                 ]; */
    # Entrypoint = [ "sleep" "1d" ];
    Env = [];
    Volumes = { "/snapcast" = {}; };
  };
}
