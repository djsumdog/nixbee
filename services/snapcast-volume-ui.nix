{ pkgs, lib, fetchurl, ... }:
let
snapcast-volume-ui-config = pkgs.writeTextFile {
    name = "snapcast-volume-ui-config";
    destination = "/snap-cast-volume-ui/config.yml";
    text = ''
    appname: "snapdance"
    layout: "main"
    template: "template_toolkit"
    engines:
      template:
        template_toolkit:
          start_tag: '<%'
          end_tag:   '%>'
    snapcast:
      demo: 0
      server:
         host: 'natalie'
         port: 1705
      rooms:
        '00:60:dd:45:69:72':
           color: '#0C0'
           name:  'Living Room'
        '90:00:4e:b8:18:6a':
          color: '#0A0'
          name: 'Dining Room'
        '80:3f:5d:05:28:02':
          color: '#040'
          name: 'Office'
    '';
};

TestLib = pkgs.perlPackages.buildPerlPackage {
  pname = "Test-Lib";
  version = "0.003";
  src = fetchurl {
    url = "mirror://cpan/authors/id/H/HA/HAARG/Test-Lib-0.003.tar.gz";
    sha256 = "d84b48d92567cba3d0afb1e8175aab836bfa8a838e19ac9080cabc2e3f9dc9f5";
  };
  meta = {
    description = "Use libraries from a t/lib directory";
    license = with lib.licenses; [ artistic1 gpl1Plus ];
  };
};


CLIOsprey = pkgs.perlPackages.buildPerlModule rec {
  pname = "CLI-Osprey";
  version = "0.08";
  src = fetchurl {
    url = "mirror://cpan/authors/id/A/AR/ARODLAND/CLI-Osprey-0.08.tar.gz";
    sha256 = "f7480523dbdf2c2b53789c06a8bd91cf6579bb8cc5678e72ea8a1faaab30d19a";
  };
  buildInputs = [
    pkgs.perlPackages.CaptureTiny
    pkgs.perlPackages.ModuleBuildTiny
    TestLib
  ];
  propagatedBuildInputs = [
    pkgs.perlPackages.GetoptLongDescriptive
    pkgs.perlPackages.ModuleRuntime
    pkgs.perlPackages.Moo
    pkgs.perlPackages.PathTiny
  ];
  meta = {
    homepage = "http://metacpan.org/release/CLI-Osprey";
    description = "MooX::Options + MooX::Cmd + Sanity";
    license = with lib.licenses; [ artistic1 gpl1Plus ];
  };
};

Dancer2 = pkgs.perlPackages.buildPerlPackage rec {
  pname = "Dancer2";
  version = "0.400000";
  src = fetchurl {
    url = "mirror://cpan/authors/id/C/CR/CROMEDOME/${pname}-${version}.tar.gz";
    sha256 = "e6cf7a5eed29261087d2ce6db631d28b4e870cb14f31c2dfa2f77c0163bc435c";
  };
  buildInputs = [
    pkgs.perlPackages.CaptureTiny
    pkgs.perlPackages.FileShareDirInstall
    pkgs.perlPackages.HTTPCookies
    pkgs.perlPackages.HTTPMessage
    pkgs.perlPackages.TestEOL
    pkgs.perlPackages.TestFatal
  ];
  propagatedBuildInputs = [
    CLIOsprey
    pkgs.perlPackages.Clone
    pkgs.perlPackages.ConfigAny
    pkgs.perlPackages.ExporterTiny
    pkgs.perlPackages.FileShare
    pkgs.perlPackages.FileWhich
    pkgs.perlPackages.HTTPDate
    pkgs.perlPackages.HTTPHeadersFast
    pkgs.perlPackages.HashMergeSimple
    pkgs.perlPackages.HashMultiValue
    pkgs.perlPackages.ImportInto
    pkgs.perlPackages.JSONMaybeXS
    pkgs.perlPackages.ModuleRuntime
    pkgs.perlPackages.Moo
    pkgs.perlPackages.PathTiny
    pkgs.perlPackages.Plack
    pkgs.perlPackages.PlackMiddlewareFixMissingBodyInRedirect
    pkgs.perlPackages.PlackMiddlewareRemoveRedundantBody
    pkgs.perlPackages.RefUtil
    pkgs.perlPackages.RoleTiny
    pkgs.perlPackages.SafeIsa
    pkgs.perlPackages.SubQuote
    pkgs.perlPackages.TemplateTiny
    pkgs.perlPackages.TemplateToolkit
    pkgs.perlPackages.TypeTiny
    pkgs.perlPackages.URI
    pkgs.perlPackages.YAML
  ];
  meta = {
    homepage = "http://perldancer.org/";
    description = "Lightweight yet powerful web application framework";
    license = with lib.licenses; [ artistic1 gpl1Plus ];
  };
};

snapcast-volume-ui-src = fetchGit {
  url = "https://github.com/atoomic/snapcast-volume-ui.git";
  ref = "snapcast/v0.12";
  rev = "5eb4076607aab7e69b08a96b902029bd1091c056";
};

snapcast-volume-ui-dir = pkgs.runCommand "" {
  paths = [ snapcast-volume-ui-src ];
} ''
  mkdir -p $out/snap-cast-volume-ui
  cp -r ${snapcast-volume-ui-src}/* $out/snap-cast-volume-ui
  '';

SimpleAccessor = pkgs.perlPackages.buildPerlPackage {
  pname = "Simple-Accessor";
  version = "1.13";
  src = fetchurl {
    url = "mirror://cpan/authors/id/A/AT/ATOOMIC/Simple-Accessor-1.13.tar.gz";
    sha256 = "5af6066c7a9f58ab963e36d12052bcc5b20e80ee32f3899ea882a79192882226";
  };
  meta = {
    description = "A light and simple way to provide accessor in perl";
    license = with lib.licenses; [ artistic1 gpl1Plus ];
  };
};

perl = pkgs.perl538;
perl-with-packages = perl.withPackages (p: with p; [
    pkgs.perlPackages.TestMoreUTF8
    pkgs.perlPackages.HTTPMessage
    pkgs.perlPackages.TestSimple13
    pkgs.perlPackages.JSONXS
    pkgs.perlPackages.NetTelnet
    pkgs.perlPackages.HTTPParserXS
    SimpleAccessor
    Dancer2
]);

in
pkgs.dockerTools.buildLayeredImage {
  name = "snapcast-volume-ui";
  contents = [ pkgs.busybox snapcast-volume-ui-dir snapcast-volume-ui-config perl-with-packages
              ];
  config = {
    Entrypoint = [ "plackup" "-p" "1705" "-R" "/snap-cast-volume-ui" "snap-cast-volume-ui/bin/app.psgi" ];
    Env = [];
    Volumes = {};
  };
}
