{ pkgs, lib, ... }:
let
  es-config = pkgs.writeTextFile {
      name = "es-config";
      destination = "/etc/elasticsearch.yml";
      text = ''
        cluster.name: fedisearch
        node.name: node-home-1
        path.logs: /elasticsearch/log
        path.data: /elasticsearch/data
        network.host: 0.0.0.0
        http.port: 9200
      '';
  };
in
pkgs.dockerTools.buildLayeredImage {
  name = "elasticsearch";
  contents = [ pkgs.elasticsearch pkgs.busybox pkgs.temurin-jre-bin-17 es-config ];
  config = {
    Entrypoint = [ "sleep" "1d" ];
    # Entrypoint = [ "/sbin/elasticsearch" ];
    Env = [ "ES_HOME=${pkgs.elasticsearch.out}" "ES_PATH_CONFIG=/etc/elasticsearch.yml" ];
    Volumes = { "/elasticsearch" = {}; };
  };
}
