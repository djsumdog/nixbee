let
  yacy = fetchGit {
    url = "https://github.com/yacy/yacy_search_server.git";
    ref = "master";
    rev = "cdb98b67e873ee26c9b70593dae23aa19c0df60b";
  };
in
stdenv.mkDerivation {
  name = "...";
  src = fetchurl { ... };

  nativeBuildInputs = [ jdk ant ];

  buildPhase = "ant";
}
