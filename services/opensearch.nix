{ pkgs, ... }:
let
opensearch-config = pkgs.writeTextFile {
    name = "opensearch-config";
    destination = "/etc/opensearch.yml";
    text = ''
      cluster.name: fedisearch
      node.name: node-home-1
      discovery.type: single-node
      path.logs: /opensearch/log
      path.data: /opensearch/data
      network.host: 0.0.0.0
      http.port: 9200
      plugins.security.disabled: true

      plugins.security.ssl.http.pemtrustedcas_filepath: /opensearch/config/certs/root-ca.pem
      plugins.security.ssl.transport.pemcert_filepath: /opensearch/config/certs/node1.pem
      plugins.security.ssl.transport.pemkey_filepath: /opensearch/config/certs/node1-key.pem
      plugins.security.ssl.transport.pemtrustedcas_filepath: /opensearch/config/certs/root-ca.pem
      # cluster.initial_cluster_manager_nodes: localhost
      plugins.security.authcz.admin_dn:
        - 'CN=Admin,OU=fedisearch,O=battlepenguin'
      plugins.security.nodes_dn:
        - 'CN=node1.dns.a-record,OU=fedisearch,O=battlepenguin'
        - 'CN=node2.dns.a-record,OU=fedisearch,O=battlepenguin'
    '';
};

opensearch-startup = pkgs.writeTextFile {
    name = "opensearch-startup";
    executable = true;
    destination = "/bin/opensearch-startup";
    text = ''
    #!/bin/sh

    echo "Opensearch Setup"
    touch /etc/passwd
    touch /etc/group
    mkdir -p /tmp
    chmod 0777 /tmp
    mkdir -p /opensearch
    adduser -u 1655 -h /opensearch opensearch
    mkdir -p $OPENSEARCH_HOME/logs
    if [ ! -d "/opensearch/config" ]; then
      echo "No configuration found. Copying initial configuration"
      cp -r $OPENSEARCH_HOME/config /opensearch
      chmod 750 /opensearch/config
      echo "Copying default configuration YML"
      cp /etc/opensearch.yml /opensearch/config/opensearch.yml
      mkdir -p /opensearch/config/certs
      cd /opensearch/config/certs
      /bin/cert-gen
      # This logging option is set for Java 9+ but does not work on recent version
      sed -i '/^[^#]/ s/\(^.*9-:-Xlog.*$\)/#\1/' /opensearch/config/jvm.options
    fi
    mkdir -p $OPENSEARCH_HOME/extensions
    chown -R opensearch:opensearch /opensearch
    chmod 755 $OPENSEARCH_HOME/*
    exec su opensearch -c /bin/opensearch
    '';
};
cert-gen = pkgs.writeTextFile {
    name = "cert-gen";
    executable = true;
    destination = "/bin/cert-gen";
    text = ''
    #!/bin/sh
    # Root CA
    openssl genrsa -out root-ca-key.pem 2048
    openssl req -new -x509 -sha256 -key root-ca-key.pem -subj "/O=battlepenguin/OU=fedisearch/CN=root.dns.a-record" -out root-ca.pem -days 3650
    # Admin cert
    openssl genrsa -out admin-key-temp.pem 2048
    openssl pkcs8 -inform PEM -outform PEM -in admin-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out admin-key.pem
    openssl req -new -key admin-key.pem -subj "/O=battlepenguin/OU=fedisearch/CN=Admin" -out admin.csr
    openssl x509 -req -in admin.csr -CA root-ca.pem -CAkey root-ca-key.pem -CAcreateserial -sha256 -out admin.pem -days 3650
    # Node cert 1
    openssl genrsa -out node1-key-temp.pem 2048
    openssl pkcs8 -inform PEM -outform PEM -in node1-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out node1-key.pem
    openssl req -new -key node1-key.pem -subj "/O=battlepenguin/OU=fedisearch/CN=node1.dns.a-record" -out node1.csr
    echo 'subjectAltName=DNS:node1.dns.a-record,DNS:localhost.localdomain,IP.1:0:0:0:0:0:0:0:1,IP.2:127.0.0.1' > node1.ext
    openssl x509 -req -in node1.csr -CA root-ca.pem -CAkey root-ca-key.pem -CAcreateserial -sha256 -out node1.pem -days 3650 -extfile node1.ext
    # Node cert 2
    openssl genrsa -out node2-key-temp.pem 2048
    openssl pkcs8 -inform PEM -outform PEM -in node2-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out node2-key.pem
    openssl req -new -key node2-key.pem -subj "/O=battlepenguin/OU=fedisearch/CN=node2.dns.a-record" -out node2.csr
    echo 'subjectAltName=DNS:node2.dns.a-record,DNS:localhost.localdomain,IP.1:0:0:0:0:0:0:0:1,IP.2:127.0.0.1' > node2.ext
    openssl x509 -req -in node2.csr -CA root-ca.pem -CAkey root-ca-key.pem -CAcreateserial -sha256 -out node2.pem -days 3650 -extfile node2.ext
    # Client cert
    openssl genrsa -out client-key-temp.pem 2048
    openssl pkcs8 -inform PEM -outform PEM -in client-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out client-key.pem
    openssl req -new -key client-key.pem -subj "/O=battlepenguin/OU=fedisearch/CN=client.dns.a-record" -out client.csr
    echo 'subjectAltName=DNS:client.dns.a-record,DNS:localhost.localdomain,IP.1:0:0:0:0:0:0:0:1,IP.2:127.0.0.1' > client.ext
    openssl x509 -req -in client.csr -CA root-ca.pem -CAkey root-ca-key.pem -CAcreateserial -sha256 -out client.pem -days 3650 -extfile client.ext
    # Cleanup
    rm admin-key-temp.pem
    rm admin.csr
    rm node1-key-temp.pem
    rm node1.csr
    rm node1.ext
    rm node2-key-temp.pem
    rm node2.csr
    rm node2.ext
    rm client-key-temp.pem
    rm client.csr
    rm client.ext
    '';
};
in
pkgs.dockerTools.buildLayeredImage {
  name = "opensearch";
  contents = [ pkgs.opensearch opensearch-startup opensearch-config cert-gen pkgs.openssl pkgs.cacert pkgs.busybox pkgs.temurin-jre-bin-17 ];
  config = {
    Entrypoint = [ "/bin/opensearch-startup" ];
    #Entrypoint = [ "sleep" "1d" ];
    Env = [ "OPENSEARCH_HOME=${pkgs.opensearch.out}" "OPENSEARCH_PATH_CONF=/opensearch/config" ];
    Volumes = { "/opensearch" = {}; };
  };
}
