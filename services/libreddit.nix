{ pkgs, ... }:
pkgs.dockerTools.buildLayeredImage {
  name = "libreddit";
  contents = [ pkgs.libreddit pkgs.cacert ];
  config = {
    Entrypoint = [ "libreddit" ];
    Env = [];
    Volumes = {};
  };
}

