{ pkgs, ... }:
let
  searx-conf = pkgs.writeTextFile {
      name = "searx-conf";
      executable = true;
      destination = "/etc/searx.conf";
      text = ''
        use_default_settings: True
        server:
            port : 8888
            bind_address : "0.0.0.0"
            secret_key : "yeiy2eeb3eeGahr3oohee0beef8chai4eifosei6gair3"
        engines:
          - name: google
            use_mobile_ui: true
      '';
  };
in
pkgs.dockerTools.buildLayeredImage {
  name = "searxng";
  contents = [ pkgs.searxng pkgs.cacert searx-conf ];
  config = {
    Entrypoint = [ "searx-run" ];
    Env = [ "SEARX_SETTINGS_PATH=/etc/searx.conf" ];
    Volumes = {};
  };
}
