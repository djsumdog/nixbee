{ pkgs, ... }:
pkgs.dockerTools.buildLayeredImage {
  name = "container-registry";
  contents = [ pkgs.docker-distribution ];
  config = {
    Entrypoint = [ "registry" ];
    Env = [];
    Volumes = {};
  };
}

