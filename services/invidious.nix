{ pkgs, ... }:
pkgs.dockerTools.buildLayeredImage {
    name = "invidious";
    contents = [ pkgs.busybox pkgs.bash pkgs.invidious ];
    config = {
      Cmd = [ "/bin/bash" ];
      Env = [];
      Volumes = {};
    };
}
